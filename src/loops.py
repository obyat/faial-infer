from traverse import *
from c_ast import *

__all__ = ["rewrite_loops"]

"""
Structured loop inference.
"""

def is_plus_k(obj):
    return is_kind(obj, "UnaryOperator") and obj.get("opcode", None) == "++" \
        or is_kind(obj, "CompoundAssignOperator") and obj.get("opcode", None) == "+="

def range_expr(init=integer_literal(0), upper_bound=None, step=integer_literal(1), opcode="+"):
    return {
        "kind": "RangeExpr",
        "init": init,
        "upper_bound": upper_bound,
        "step": step,
        "opcode": opcode,
    }

def for_each_stmt(var, range, body):
    return {
        "kind": "ForEachStmt",
        "var": var,
        "range": range,
        "body": body
    }

FORWARD = {"++", "+=", "<<=", "*="}
BACKWARD = {"--", "-=", ">>=", "/="}

def infer_range(obj):
    """
    The simplest loop example is `for (int i = 0; i < 100; i++)`:

    >>> loop = for_stmt(
    ... init=decl_stmt_var("i", init=integer_literal(0)),
    ... cond=lt_operator(
    ...    var_decl("i", type=int_type()),
    ...     integer_literal(100)),
    ... inc=plus_plus_var("i"),
    ... body=compound_stmt(),
    ... )
    >>> var, rng = infer_range(loop)

    The result is a variable and a loop.

    The inferred variable should be `i`:

    >>> var == var_decl("i", type=int_type())
    True

    The range should trivially inferred:

    >>> rng == range_expr(
    ... init = integer_literal(0),
    ... upper_bound = integer_literal(100),
    ... opcode = '+',
    ... step = integer_literal(1),
    ... )
    True

    """
    ub = obj["cond"]["rhs"]
    init = obj["init"]

    if is_decl_stmt(init):
        var, = obj["init"]["inner"]
        init, = var["inner"]
        del var["inner"]
        del var["init"]
    elif is_kind(init, "BinaryOperator") and init.get('opcode', None) == '=':
        var = init["lhs"]
        init = init["rhs"]
    else:
        raise ValueError("Unsupported initialization pattern %r" % init)

    step = obj["inc"]
    if step.get("opcode", None) in BACKWARD:
        ub, init = init, ub
        ub = inc_operator(ub)
        if obj["cond"].get("opcode", None) == ">":
            init = inc_operator(init)
    if obj["cond"]["opcode"] == "<=":
        ub = inc_operator(ub)

    if step.get("opcode", None) in {">>=", "<<="}:
        opcode = "pow2"
    else:
        opcode = "+"

    if is_kind(step, "UnaryOperator") and step.get("opcode", None) in {"++", "--"}:
        step = integer_literal(1)
    elif is_kind(step, "CompoundAssignOperator"):
        if step["opcode"] in {"*=", "/="} and is_kind(step["rhs"], "IntegerLiteral") and step["rhs"]["value"] > 0:
            opcode = "pow%d" % step["rhs"]["value"]
            step = integer_literal(1)
        else:
            step = step["rhs"]
    else:
        raise ValueError(repr(step))

    rng = range_expr(
        init = init,
        upper_bound = ub,
        step = step,
        opcode = opcode,
    )
    return var, rng

def infer_foreach(obj):
    var, rng = infer_range(obj)
    return for_each_stmt(
        var = var,
        range = rng,
        body = obj["body"]
    )

def rewrite_loops(obj):
    for child in filter(is_for_stmt, walk(obj)):
        become(child, infer_foreach(child))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
