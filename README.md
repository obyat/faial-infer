# Development

For testing we use [pytest](https://docs.pytest.org/en/latest/).

To run the tests of our project, just run:

```bash
$ pytest
```

Test modules are prefixed with `test_`.